echo "create database test" | docker exec -i stack-grafana-influxdb_mysql_1 mysql 

echo "drop table if exists temperatura; create table temperatura( ts bigint, localidad varchar(255), value float, primary key(ts,localidad));" | docker exec -i stack-grafana-influxdb_mysql_1 mysql test 

#CREATE USER 'root'@'%' IDENTIFIED BY 'root'; 
echo "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;" \
   | docker exec -i stack-grafana-influxdb_mysql_1 mysql test

cat autocommit_off.txt to_load_mysql.txt commit.txt| docker exec -i stack-grafana-influxdb_mysql_1 mysql test
