const fs = require('fs');
const CANT_MINUTOS= 60*24*12; //12 days 

let ws_influx = fs.createWriteStream('to_load_influx.txt');
let ws_mysql = fs.createWriteStream('to_load_mysql.txt');

const ts_start =  Math.round(new Date().getTime() / 1000) - (CANT_MINUTOS*60);
// write some data with a base64 encoding
for(let i=1; i<CANT_MINUTOS;i++) {

    const temp=20 + Math.random(3) * 5;
    const ts = ts_start + i*60
    ws_influx.write(`temperatura,localidad=Salto valor=${temp} ${ts*10**9}\n`);
    ws_mysql.write(`INSERT INTO temperatura VALUES(${ts}, 'Salto',${temp});\n`);
}

// the finish event is emitted when all data has been flushed from the stream
ws_influx.on('finish', () => {
    console.log('fin');
});
ws_mysql.on('finish', () => {
    console.log('fin');
});

// close the stream
ws_influx.end();
ws_mysql.end();

