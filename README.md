# Stack Grafana + InfluxDB + Telegraf

Este repositorio contiene lo minimo requerido para correr un stack con 

* [InfluxDB](https://www.influxdata.com/products/influxdb-overview/) Base de datos orientada a series temporales Open Source
* [Grafana](https://grafana.com/) Tableros de monitoreo Open Source
* [Telegraf](https://www.influxdata.com/time-series-platform/telegraf/) Agente recolector de metricas Open Source

## Run

Clonar repositorio

> git clone git@gitlab.com:dgraselli/stack-grafana-influxdb.git

Ejecutar el stack

> cd stack-grafana-influxdb
>
> docker-compose up -d

Si todo esta OK, ya se puede ver [Grafana](http://localhost:3000)

Para ejecutar influxDB

> docker exec -it [contanier_name] influx 

Crear la base de datos "telegraf" y "db1"

> create datbase telegraf
> create datbase db1

Pruebas con curl

> cat test_curl.sh

Pruebas con bash

> test_bash.sh

## Stop

> docker-compose stop

## Remvoe

> docker-compose down

## Load DATA to InfluxDB

> bash load_influxdb.sh

## Load DATA to Mysql

> bash load_mysq.sh

## Generate new data

Ver y editar *generate_data.js*

> node generate_data.js

