const axios = require("axios");
const Influx = require("influx");

const influx = new Influx.InfluxDB("http://localhost:8086/mydb");

const getPrice = () => {
  axios
    .get("https://api.coindesk.com/v1/bpi/currentprice.json")
    .then((res) => {
      let usd = parseFloat(res.data.bpi.USD.rate.replace(",", ""));
      let eur = parseFloat(res.data.bpi.EUR.rate.replace(",", ""));
      //ruidito
      usd += Math.random()* 10; 
      eur += Math.random()* 10;

      //console.log(`USD: ${usd} EUR: ${eur}`);
      influx.writePoints(
        [
          {
            measurement: "bitcoin",
            tags: { moneda: "USD" },
            fields: { valor: usd },
          },
          {
            measurement: "bitcoin",
            tags: { moneda: "EUR" },
            fields: { valor: eur },
          },
        ],
        { precision: "s" }
      );
    })
    .catch((e) => {
      console.log("Error getting data");
      console.log(e);
    });

  setTimeout(getPrice, 1);
};

getPrice();
