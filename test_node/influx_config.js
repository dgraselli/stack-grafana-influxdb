const Influx = require("influx");
const URL= require('url').URL

const influx =  new Influx.InfluxDB('http://localhost:8086/test');

exports.writeData = (tags, values, m) => {
  influx
    .writePoints(
      [
        {
          measurement: m,
          tags: tags,
          fields: values
        }
      ],
      {
        //database: 'test',
        precision: 's'
      }
    )
    .then(e=>{
      console.error("OK: " + e);
    })
    .catch(err => {
      console.error("Error writing data to Influx: " + err);
    });
};

