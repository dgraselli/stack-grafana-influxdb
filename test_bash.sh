
# curl -i -XPOST http://localhost:8086/query --data-urlencode 'q=create database mydb'


for i in {1..100}
do
 T=$(( RANDOM % 100 /10 + 20))
 H=$(( RANDOM % 100 /10 + 80))

curl -i -XPOST 'http://localhost:8086/write?db=mydb' --data-binary \
 "medidas_repetida,localidad=25Mayo,prov=BSAS temp=${T},hum=${H}"
sleep 1
done


#>  docker exec -it influxdb1 influx -database mydb
#>  select *from medidas;

# SELECT FROM MEDIDAS
#curl -i -XPOST http://localhost:8086/query?db=mydb --data-urlencode 'q=select * from medidas'

# WRITE 1
#curl -i -XPOST http://localhost:8086/write?db=mydb --data-binary 'medidas,localidad=Salto,prov=BSAS temp=1,hum=2'


# EN BROWSER
# http://localhost:8086/query?db=mydb&q=select%20*%20from%20medidas

# INFLUX CONSOLE
# docker exec -it stack-grafana-influxdb_influxdb_1 influx
# use mydb
# show measurements
# select * from medidas limit 5;
# select avg(temp) from medidas group by time(1m) limite 5;

#CONSOLA
#> docker exec -it stack-grafana-influxdb_influxdb_1 influx